import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NotFound from "../components/NotFound";
import HomePage from "../containers/HomePage";
import Footer from "../components/partials/Footer";
import Header from "../components/partials/Header";
import AlbumDetails from "../containers/AlbumDetails";
import ArtistDetails from "../containers/ArtistDetails";
import AddForm from "../components/AddForm";
import EditForm from "../containers/EditForm";

const AppRouter = props => {
  return (
    <div>
      <BrowserRouter>
        <div className="container">
          <Header />
          <div className="container__app" >
            <Switch>
              <Route path="/albums/:id" component={AlbumDetails} />
              <Route path="/singers/:id" component={ArtistDetails} />
              <Route path="/add" component={AddForm} />
              <Route path="/edit/:id" component={EditForm} />
              <Route path="/" component={HomePage} />
              <Route component={NotFound} />
            </Switch>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    </div>
  );
};

export default AppRouter;
