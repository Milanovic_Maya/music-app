import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { image } from '../shared/constants';
import { Link } from "react-router-dom";


const styles = theme => ({
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 200,
        height: 200,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
    },
    playIcon: {
        height: 38,
        width: 38,
    },
});

const SingleAlbum = props => {
    const { classes, album } = props;

    return (
        <Card className="single-album">
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography variant="headline">
                        <Link to={`/albums/${album.id}`}>
                            <p>
                                {album.name}
                            </p>
                        </Link>
                    </Typography>
                    <Typography variant="subheading" color="textSecondary">
                        <p>
                            Artist : {album.description}
                        </p>
                    </Typography>
                    <Typography variant="subheading" color="textSecondary">
                        Year: {album.date}
                    </Typography>
                </CardContent>
            </div>
            <CardMedia
                className={classes.cover}
                image={image}
                title={album.fileImg}
            />
        </Card>
    );
}

SingleAlbum.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(SingleAlbum);
