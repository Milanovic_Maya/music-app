import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import RemoveIcon from '@material-ui/icons/Remove';
import { withRouter } from "react-router-dom";

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    }
});

const RemoveButton = props => {
    const { classes, item, removeItem, history } = props;
    const onRemoveItem = e => {
        removeItem(item.id);
        history.push("/");
    };

    return (
        <div className="remove-button">
            <Button
                onClick={onRemoveItem}
                className={classes.button}
            >
                <RemoveIcon />
                Remove
            </Button>
        </div>
    );
}

export default withStyles(styles)(withRouter(RemoveButton));
