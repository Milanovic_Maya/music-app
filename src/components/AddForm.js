import React from "react";
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputFields from "../containers/InputFields";

const styles = theme => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing.unit * 3,
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
    },
});

class AddForm extends React.Component {
    state = {
        value: 'album'
    };

    handleChange = event => {
        this.setState({ value: event.target.value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        return (
            <div className="feed">
                <div className={classes.root}>
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="legend">You want to add...</FormLabel>
                        <RadioGroup
                            aria-label="add"
                            name="add2"
                            className={classes.group}
                            value={this.state.value}
                            onChange={this.handleChange}
                        >
                            <FormControlLabel
                                value="album"
                                control={<Radio color="primary" />}
                                label="Album"
                                labelPlacement="end"
                            />
                            <FormControlLabel
                                value="artist"
                                control={<Radio color="primary" />}
                                label="Artist"
                                labelPlacement="end"
                            />
                        </RadioGroup>
                    </FormControl>
                </div>
                <InputFields display={value} id=""/>
            </div>
        );
    }
}
export default withStyles(styles)(AddForm);
