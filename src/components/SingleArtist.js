import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { image } from '../shared/constants';
import { Link } from "react-router-dom";

const styles = theme => ({
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: "100 %",
        height: "120px",
        backgroundSize: "cover"
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
    },
    playIcon: {
        height: 38,
        width: 38,
    },
});

const SingleArtist = props => {
    const { classes, artist } = props;

    return (
        <Card className="single-artist">
            <CardMedia
                className={classes.cover}
                image={image}
                title={artist.fileImg}
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography variant="subheading" color="textSecondary">
                        <Link to={`/singers/${artist.id}`}>
                            <p>{artist.name}</p>
                        </Link>
                    </Typography>
                </CardContent>
            </div>
        </Card>
    );
}

SingleArtist.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(SingleArtist);
