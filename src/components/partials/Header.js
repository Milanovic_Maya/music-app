import React from "react";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import { headerMessage } from "../../shared/constants";

const Header = props => {
    return (
        <div className="header">
            <div className="header__container">
                <div className="header__container__icon">
                    <PlayArrowIcon />
                </div>
                <div className="header__container__message">{headerMessage}</div>
            </div>
        </div>
    )
};

export default Header;