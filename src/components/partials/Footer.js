import React, { Fragment } from "react";
import { copyrigthFooterMessage } from "../../shared/constants";

const Footer = () => (
    <Fragment>
        <div className="above-footer"></div>
        <div className="footer">
            <div className="footer__container">
                <p className="footer__container--copyright-message">
                    {copyrigthFooterMessage}
                </p>
            </div>
        </div>
    </Fragment>
)

export default Footer;