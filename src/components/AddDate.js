import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: "200",
    },
});

const AddDate = (props) => {
    const { classes, getDate, label } = props;
    return (
        <form className={classes.container} noValidate>
            <TextField
                id="date"
                label={label}
                type="date"
                onChange={getDate}
                className={classes.textField}
                InputLabelProps={{
                    shrink: true,
                }}
            />
        </form>
    );
}
export default withStyles(styles)(AddDate);
