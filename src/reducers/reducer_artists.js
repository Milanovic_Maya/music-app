import {
    SET_ARTIST_LIST,
    CREATE_ARTIST,
    EDIT_ARTIST,
    REMOVE_ARTIST
} from "../actions/action_types";

const defaultState = [];

export default (state = defaultState, action) => {
    switch (action.type) {
        case SET_ARTIST_LIST:
            return [
                ...state,
                action.artists
            ];
        case CREATE_ARTIST:
            return [
                ...state,
                action.artistData
            ];
        case EDIT_ARTIST:
            return state.map(artist => {
                if (artist.id === action.id) {
                    return {
                        ...artist,
                        ...action.updates
                    };
                } else {
                    return artist;
                }
            });
        case REMOVE_ARTIST:
            return state.filter(artist => {
                return artist.id !== action.id
            });
        default:
            return state;
    }
};