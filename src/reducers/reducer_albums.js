import {
    SET_ALBUM_LIST,
    CREATE_ALBUM,
    EDIT_ALBUM,
    REMOVE_ALBUM
} from "../actions/action_types";

const defaultState = [];

export default (state = defaultState, action) => {
    switch (action.type) {
        case SET_ALBUM_LIST:
            return [
                ...state,
                action.albums
            ];
        case CREATE_ALBUM:
            return [
                ...state,
                action.albumData
            ];
        case EDIT_ALBUM:
            return state.map(album => {
                if (album.id === action.id) {
                    return {
                        ...album,
                        ...action.updates
                    };
                } else {
                    return album;
                }
            });
        case REMOVE_ALBUM:
            return state.filter(album => {
                return album.id !== action.id
            });
        default:
            return state;
    }
};