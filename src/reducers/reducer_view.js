import {
    ALBUMS_VIEW,
    SINGERS_VIEW,
    // FORM_VIEW
} from "../actions/action_types";

const defaultState = {
    value: 0
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case ALBUMS_VIEW:
            return {
                ...state,
                value: 0
            };
        case SINGERS_VIEW:
            return {
                ...state,
                value: 1
            };
        // case FORM_VIEW:
        //     return {
        //         ...state,
        //         value: 2
        //     };
        default:
            return state;
    }
};