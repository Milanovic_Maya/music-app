import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './router/AppRouter';
import "./styles/css/styles.css";
import "normalize.css";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import { saveState } from "./services/storageService";
import registerServiceWorker from './registerServiceWorker';

const store = configureStore();

store.subscribe(() => {
    saveState({
        view: store.getState().view,
        albums: store.getState().albums,
        artists: store.getState().artists
    });
});

const App = () => (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
