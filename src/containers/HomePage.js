import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AlbumList from "../containers/AlbumList";
import ArtistList from "../containers/ArtistList";
import { albumsView, singersView } from "../actions/action_view";
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    }
});

class HomePage extends Component {
    state = {
        value: this.props.value ? this.props.value : 0
    };

    toggleHomePageView = (event, value) => {
        this.setState({ value });
        if (value === 0) {
            this.props.albumsView();
        };
        if (value === 1) {
            this.props.singersView();
        };
    };

    render() {
        const { value } = this.state;
        const { classes } = this.props;
        return (
            <div>
                <div className="home-page__container">
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.toggleHomePageView}>
                            <Tab label="Albums" />
                            <Tab label="Singers" />
                        </Tabs>
                    </AppBar>
                    {value === 0 && <AlbumList />}
                    {value === 1 && <ArtistList />}
                    <div className="add-button">
                        <Link to="/add">
                            <Button variant="fab" color="primary" aria-label="Add" className={classes.button}>
                                <AddIcon />
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = state => ({
    value: state.view.value
});

const mapDispatchToProps = dispatch => ({
    albumsView: () => dispatch(albumsView()),
    singersView: () => dispatch(singersView()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(HomePage));

