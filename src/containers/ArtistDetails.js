import React from "react";
import { Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { image } from "../shared/constants";
import EditIcon from '@material-ui/icons/Edit';
import { connect } from "react-redux";
import RemoveButton from "../components/RemoveButton";
import { removeArtist } from "../actions/action_artists";

const styles = {
    title: {
        marginBottom: 16,
        fontSize: 20,
    },
    pos: {
        marginBottom: 12,
    },
};

const ArtistDetails = props => {
    const { classes, removeArtist } = props;
    const artist = props.artist[0];
    return (
        <div className="artist">
            <div>
                {artist ? <Card className="artist-details">
                    <div className="artist-details__container">
                        <CardContent className="artist-details__container__content">
                            <Typography className={classes.title} color="textSecondary">
                                {artist.name}
                            </Typography>

                            <Link to={`/edit/${artist.id}`}>
                                <Typography component="p"
                                    className="artist-details__container__content__edit"
                                >
                                    <EditIcon />
                                    EDIT
                          </Typography>
                            </Link>
                            <RemoveButton item={artist} removeItem={removeArtist} />
                        </CardContent>
                    </div>
                    <Card className="artist-details__container__content__about">
                        <CardMedia
                            className="artist-details__container__thumb"
                            image={image}
                            title={artist.fileImg}
                        />
                        <div className="artist-details__container__about__info">
                            <p>
                                {artist.description}
                            </p>
                            <p>
                                Birthday: {artist.date}
                            </p>
                            <p>
                                {/*if exist this data???*/}
                                ALBUMS FROM THIS ARTIST
                                <dl>
                                    <dt>ALBUM1</dt>
                                    <dt>ALBUM2</dt>
                                    <dt>ALBUM3</dt>
                                </dl>
                            </p>
                        </div>
                    </Card>
                </Card>
                    : <p className="no-data-message">No artist selected.</p>
                }
            </div>
        </div>
    );
};

const mapStateToProps = (state, props) => ({
    artist: state.artists.filter(artist => {
        return artist.id === props.match.params.id
    })
});

const mapDispatchToProps = dispatch => ({
    removeArtist: id => dispatch(removeArtist(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ArtistDetails));
