import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import AddDate from "../components/AddDate";
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import { createAlbum, editAlbum } from "../actions/action_albums";
import { createArtist, editArtist } from "../actions/action_artists";
// import { filterStateByID } from "../shared/utils";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        paddingRight: "1rem",
        paddingLeft: "1rem"
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    formControl: {
        margin: theme.spacing.unit,
        width: "100%"
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
});

class InputFields extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.item ? this.props.item.name : "",
            date: this.props.item ? this.props.item.date : "",
            description: this.props.item ? this.props.item.description : "",
            fileImg: this.props.item ? this.props.item.fileImg : ""
        };

    };

    onNameChange = e => {
        const name = e.target.value;
        this.setState({ name });
    };

    onDescriptionChange = e => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };

    getDate = e => {
        const date = moment(e.target.value).format("Do MM YYYY");
        this.setState(() => ({ date }))
    };
    onButtonClick = e => {
        this.clearInputs();
        this.props.history.push("/");
    };

    clearInputs = () => {
        this.setState(() => ({
            name: "",
            date: "",
            description: "",
            fileImg: ""
        }));
    };

    onCreate = e => {
        const { name, date, description, fileImg } = this.state;
        const { display, createAlbum, createArtist, item, editArtist, editAlbum } = this.props;

        if (!item) {

            if (name && date && description && fileImg.length !== 0) {
                if (display === "album") {
                    createAlbum({ name, date, description, fileImg });
                };
                if (display === "artist") {
                    createArtist({ name, date, description, fileImg });
                }
                this.onButtonClick();
            }
        } else if (item) {
            if (display === "album") {
                const { id } = item;
                const updates = { name, date, description, fileImg };
                
                editAlbum(id, updates);
            };
            if (display === "artist") {
                const { id } = item;
                const updates = { name, date, description, fileImg };

                editArtist(id, updates);
            };
            this.onButtonClick();
        }
    };

    onFilesChange = e => {
        const files = Array.from(e.target.files);
        const fileImg = files[0].name;
        this.setState(() => ({ fileImg }));
    };

    render() {
        const { classes, display } = this.props;

        return (
            <div className={classes.container}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="name-simple">Name</InputLabel>
                    <Input id="name-simple" value={this.state.name} onChange={this.onNameChange} />
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="details-simple">{display === "album" ? "Artist" : "Full Name"}</InputLabel>
                    <Input id="details-simple" value={this.state.description} onChange={this.onDescriptionChange} />
                </FormControl>
                <AddDate
                    label={display === "album" ? "Released" : "Birthday"}
                    getDate={this.getDate}
                />
                <div className="upload-button">
                    <input
                        accept="image/*"
                        className={classes.input}
                        id="contained-button-file"
                        // multiple
                        type="file"
                        onChange={this.onFilesChange}
                    />
                    <label htmlFor="contained-button-file">
                        <Button variant="contained" component="span" className={classes.button}>
                            Upload Image
                            <CloudUploadIcon className={classes.rightIcon} />
                        </Button>
                    </label>
                    {(this.state.fileImg !== "") && <p className="file-name">{this.state.fileImg}</p>}
                </div>
                <div className="control-buttons">
                    <IconButton
                        className={classes.button} aria-label="Close"
                        onClick={this.onButtonClick}
                    >
                        <CloseIcon />
                    </IconButton>
                    <IconButton
                        className={classes.button} aria-label="Create"
                        onClick={this.onCreate}
                    >
                        <NoteAddIcon />
                    </IconButton>
                </div>

            </div>
        );
    }
}

InputFields.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
    createAlbum: albumData => dispatch(createAlbum(albumData)),
    createArtist: artistData => dispatch(createArtist(artistData)),
    editAlbum: (id, updates) => dispatch(editAlbum(id, updates)),
    editArtist: (id, updates) => dispatch(editArtist(id, updates))
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(withRouter(InputFields)));
