import React from "react";
import { Link } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { image } from "../shared/constants";
import EditIcon from '@material-ui/icons/Edit';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import { connect } from "react-redux";
import RemoveButton from "../components/RemoveButton";
import { removeAlbum } from "../actions/action_albums";


const styles = {
    title: {
        marginBottom: 16,
        fontSize: 20,
    },
    pos: {
        marginBottom: 12,
    },
};
const AlbumDetails = props => {
    const { classes, removeAlbum } = props;
    const album = props.album[0];
    return (
        <div className="album">
            <div>
                {album ? <Card className="album-details">
                    <CardMedia
                        className="album-details__container__artist"
                        image={image}
                        title={album.name}
                    />
                    <Card className="album-details__container">
                        <CardMedia
                            className="album-details__container__thumb"
                            image={image}
                            title="Live from space album cover"
                        />
                        <CardContent className="album-details__container__content">
                            <Typography className={classes.title} color="textSecondary">
                                {album.name}
                            </Typography>
                            <Typography variant="headline" component="h2">
                                by {album.description}
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                released on : {album.date}
                            </Typography>
                            <Typography component="p">
                                <PlayArrowIcon />
                                SONG LIST
                            </Typography>

                            <Link to={`/edit/${album.id}`}>
                                <Typography component="p"
                                    className="album-details__container__content__edit"
                                >
                                    <EditIcon />
                                    EDIT
                                    </Typography>
                            </Link>
                            <RemoveButton item={album} removeItem={removeAlbum} />
                        </CardContent>

                    </Card>
                </Card>
                    : <p className="no-data-message">No album selected.</p>
                }
            </div>
        </div>
    );
};

const mapStateToProps = (state, props) => ({
    album: state.albums.filter(album => {
        return album.id === props.match.params.id
    })
});

const mapDispatchToProps = dispatch => ({
    removeAlbum: id => dispatch(removeAlbum(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AlbumDetails));
