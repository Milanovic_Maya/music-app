import React from "react";
import { connect } from "react-redux";
import { filterStateByID } from "../shared/utils";
import InputFields from "./InputFields";

const EditForm = props => {
    
    return (
        <div className="feed">
            {props.item && <InputFields display={props.item.display} item={props.item} />}
        </div>
    )
};

const mapStateToProps = (state, props) => ({
    item: filterStateByID(state, props.match.params.id)
});

export default connect(mapStateToProps)(EditForm);