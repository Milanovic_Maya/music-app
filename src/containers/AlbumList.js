import React from "react";
import SingleAlbum from "../components/SingleAlbum";
import { connect } from "react-redux";

const AlbumList = ({ albums }) => {
    return (
        <div>
            <ul>
                {albums.map(album => {
                    return <li key={album.id}><SingleAlbum album={album} /></li>
                })}
            </ul>
            {albums.length === 0 && <p className="no-data-message">There are no albums yet.</p>}
        </div>
    );
};

const mapStateToProps = state => ({
    albums: state.albums
});

export default connect(mapStateToProps)(AlbumList);

