import React from "react";
import SingleArtist from "../components/SingleArtist";
import { connect } from "react-redux";

const ArtistList = ({ artists }) => {
    return (
        <div className="artist-list">
            <ul>
                {artists.map(artist => {
                    return <li key={artist.id}><SingleArtist artist={artist} /></li>
                })}
            </ul>
            {artists.length === 0 && <p className="no-data-message">There are no artists yet.</p>}
        </div>
    );
};

const mapStateToProps = state => ({
    artists: state.artists
});

export default connect(mapStateToProps)(ArtistList);

