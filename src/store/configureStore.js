import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { loadState } from "../services/storageService";
import { createLogger } from 'redux-logger';

import reducer_albums from "../reducers/reducer_albums";
import reducer_view from "../reducers/reducer_view";
import reducer_artists from "../reducers/reducer_artists";

const logger = createLogger();

const persistedState = loadState();

export default () => {
  const store = createStore(
    combineReducers({
      albums: reducer_albums,
      artists: reducer_artists,
      view: reducer_view
    }),
    persistedState,
    applyMiddleware(thunk, logger)
  );
  return store;
};
