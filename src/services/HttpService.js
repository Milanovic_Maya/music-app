import axios from 'axios'

class Http {
    get(url, options = {}, params = {}) {
        return axios({
            method: 'GET',
            url,
            params,
            ...options
        })
    }

    post(url, item) {
        return axios(url, {
            method: "POST",
            data: JSON.stringify(item),
        })
    }
}

export const http = new Http()