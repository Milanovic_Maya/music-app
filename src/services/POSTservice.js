import { BASE_URL } from "../shared/endpoints";
import { http } from "./HttpService";

export const postItem = (item, endpoint) => {
    const url = `${BASE_URL}${endpoint}`
    return http.post(url, item)
        .then(response => response)
};