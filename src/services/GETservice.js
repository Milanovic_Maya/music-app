import {
    BASE_URL
} from "../shared/endpoints";
import { http } from "./HttpService";

export const getList = endpoint => {
    const api = `${BASE_URL}${endpoint}`
    return http.get(api)
        .then(response => response)
};