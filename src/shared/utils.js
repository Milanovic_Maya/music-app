export const filterStateByID = (state, id) => {
    const albums = state.albums;
    const artists = state.artists;

    let item;
    let type;

    for (let i = 0; i < albums.length; i++) {
        if (albums[i].id === id) {
            item = albums[i];
            type = "album";
        }
    };
    if (type !== "album") {
        for (let j = 0; j < artists.length; j++) {
            if (artists[j].id === id) {
                item = artists[j];
            }
        }
    };
    return item;
};