import {
    CREATE_ARTIST,
    SET_ARTIST_LIST,
    EDIT_ARTIST,
    REMOVE_ARTIST
} from "./action_types";
import {
    getList
} from "../services/GETservice";
import {
    SINGERS
} from "../shared/endpoints";


import uuidv4 from "uuid/v4";
import {
    postItem
} from "../services/POSTservice";

export const createArtist = (artistData) => ({
    type: CREATE_ARTIST,
    artistData: {
        ...artistData,
        id: uuidv4(),
        display: "artist"
    }
});

// create artist

export const startCreateArtist = (artistData = {}) => {
    return dispatch => {
        return postItem(artistData, SINGERS)
            .then(artistResponse => dispatch(createArtist(artistResponse)))
    }
};

export const setArtistList = artists => ({
    type: SET_ARTIST_LIST,
    artists
});

export const startSetArtistList = () => {
    return dispatch => {
        getList(SINGERS)
            .then(response => {
                return dispatch(setArtistList(response));
            })
    }
};

//edit artist

export const editArtist = (id, updates) => ({
    type: EDIT_ARTIST,
    id,
    updates
});

export const startEditArtist = () => {
    return dispatch => {
        // update item on server
        // then dispatch that response to store
    }
};

// remove artist

export const removeArtist = id => ({
    type: REMOVE_ARTIST,
    id
});