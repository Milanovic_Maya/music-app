import {
    SET_ALBUM_LIST,
    REMOVE_ALBUM,
    EDIT_ALBUM,
    CREATE_ALBUM
} from "./action_types";

import {
    getList
} from "../services/GETservice";
import {
    ALBUMS
} from "../shared/endpoints";
import {
    postItem
} from "../services/POSTservice";

import uuidv4 from "uuid/v4";

export const setAlbumList = albums => ({
    type: SET_ALBUM_LIST,
    albums
});

// create album

export const createAlbum = (albumData) => ({
    type: CREATE_ALBUM,
    albumData: {
        ...albumData,
        id: uuidv4(),
        display: "album"
    }
});

export const startCreateAlbum = (albumData = {}) => {
    return dispatch => {
        return postItem(albumData, ALBUMS)
            .then(albumResponse => dispatch(createAlbum({
                albumResponse
            })))
    }

};

export const startSetAlbumList = () => {
    return dispatch => {
        return getList(ALBUMS)
            .then(response => {
                return dispatch(setAlbumList(response))
            })
    }
};

// edit album

export const editAlbum = (id, updates) => ({
    type: EDIT_ALBUM,
    id,
    updates
});

export const startEditAlbum = () => {
    return dispatch => {
        // update item on server
        // then dispatch that response to store
    }
};

// remove album

export const removeAlbum = id => ({
    type: REMOVE_ALBUM,
    id
});