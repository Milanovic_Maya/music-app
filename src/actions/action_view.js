import {
    ALBUMS_VIEW,
    SINGERS_VIEW,
    // FORM_VIEW
} from "./action_types";

export const albumsView = () => ({
    type: ALBUMS_VIEW
});
export const singersView = () => ({
    type: SINGERS_VIEW
});
// export const formView = () => ({
//     type: FORM_VIEW
// });